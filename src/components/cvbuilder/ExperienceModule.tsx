import styled from "styled-components";

import AddIcon from "../../common/svg/AddIcon";
import CalendarIcon from "../../common/svg/CalendarIcon";
import LocationIcon from "../../common/svg/LocationIcon";
import RemoveIcon from "../../common/svg/RemoveIcon";
import CalendarInput from "../../common/ui/CalendarInput";
import InputWithIcon from "../../common/ui/InputWithIcon";
import SmartInput from "../../common/ui/SmartInput";

type ExperienceEntryProps = {
  isActive: boolean;
};
const ExperienceEntry = styled.div<ExperienceEntryProps>`
  border-radius: 9px;
  margin: 1px -10px;
  padding: 14px;
  background: ${({ isActive }) => (isActive ? "#fff" : "transparent")};
  position: relative;
  box-sizing: border-box;

  &:hover {
    border: 1px solid #2dc08d;
  }

  border: ${({ isActive }) =>
    isActive ? "1px solid #2dc08d" : "1px solid transparent"};
`;

const Title = styled.input`
  display: block;
  font-family: Rubik;
  font-style: normal;
  font-weight: normal;
  font-size: 22px;
  line-height: 26px;
  color: #2d3639;

  ::placeholder {
    color: #2d3639;
  }
`;

const CompanyName = styled.input`
  font-family: Rubik;
  font-style: normal;
  font-weight: 500;
  font-size: 16px;
  line-height: 22px;
  color: #3f9cdc;

  ::placeholder {
    color: #3f9cdc;
  }
`;

const PeriodAndPlaceHolder = styled.div`
  display: flex;
`;

const EntryMenu = styled.div`
  height: 38px;
  display: flex;
  align-items: center;

  position: absolute;
  left: 0;
  right: 0;
  margin: auto;
  display: flex;
  justify-content: center;
  top: -20px;
`;

const AddEntry = styled.button`
  background: #56bd94;
  border-radius: 30px 0px 0px 30px;
  border: none;
  display: flex;
  align-items: center;
  height: 100%;
  justify-content: center;
  padding: 10px;
  gap: 10px;
  cursor: pointer;
`;

const AddEntryText = styled.p`
  font-family: Inter;
  font-style: normal;
  font-weight: normal;
  font-size: 15px;
  line-height: 18px;

  color: #fff;
`;

const RemoveEntry = styled.button`
  background: #fff;
  border: none;
  height: 100%;
  border-radius: 0 30px 30px 0;
  width: 40px;
  display: flex;
  justify-content: center;
  align-items: center;
  cursor: pointer;
`;

type ExperienceModuleProps = {
  activeModule: () => void;
  addNewEntry: () => void;
  removeEntry: () => void;
  isActive: boolean;
};
export default function ExperienceModule({
  activeModule,
  addNewEntry,
  removeEntry,
  isActive,
}: ExperienceModuleProps) {
  return (
    <ExperienceEntry
      isActive={isActive}
      onClick={() => {
        if (!isActive) {
          activeModule();
        }
      }}
    >
      {isActive && (
        <EntryMenu>
          <AddEntry type="button" onClick={() => addNewEntry()}>
            <AddIcon />
            <AddEntryText>New entry</AddEntryText>
          </AddEntry>
          <RemoveEntry type="button" onClick={() => removeEntry()}>
            <RemoveIcon />
          </RemoveEntry>
        </EntryMenu>
      )}
      <Title placeholder="Title" />
      <CompanyName placeholder="Company Name" />
      <PeriodAndPlaceHolder>
        <CalendarInput Icon={<CalendarIcon />} placeholder="Date period" />
        <InputWithIcon Icon={<LocationIcon />} placeholder="New York, NY" />
      </PeriodAndPlaceHolder>
      <SmartInput placeholder="Company Description" />
    </ExperienceEntry>
  );
}
