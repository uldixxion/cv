import { useState } from "react";
import styled from "styled-components";

import CVCommonInfo from "./CVCommonInfo";
import CVExperience from "./CVExperience";

type CVPageHolderProps = {
  editMode: boolean;
};
const CVPageHolder = styled.div<CVPageHolderProps>`
  height: 1329px;
  width: 100%;
  background: ${({ editMode }) => (editMode ? "rgba(0, 0, 0, 0.2)" : "#fff")};
  box-shadow: 0px 2px 6px rgba(0, 0, 0, 0.1);
`;

const CVContent = styled.div`
  padding: 76px;
`;

export default function CVPage() {
  const [editMode, setEditMode] = useState(false);

  return (
    <CVPageHolder editMode={editMode}>
      <CVContent>
        <CVCommonInfo />
        <CVExperience editMode={editMode} setEditMode={setEditMode} />
      </CVContent>
    </CVPageHolder>
  );
}
