import styled from "styled-components";

import CVHeadInfo from "./CVHeadInfo";
import CVPage from "./CVPage";

const CVBuilderHolder = styled.div`
  width: 940px;
  margin: 60px auto 100px auto;
`;

export default function CVBuilder() {
  return (
    <CVBuilderHolder>
      <CVHeadInfo />
      <CVPage />
    </CVBuilderHolder>
  );
}
