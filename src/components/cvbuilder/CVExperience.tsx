import { useState, useRef } from "react";
import styled from "styled-components";
import { nanoid } from "nanoid";

import ExperienceModule from "./ExperienceModule";
import useClickAway from "../../common/hooks/useClickAway";

const ExperienceHolder = styled.div`
  margin: 20px 0;
  width: 480px;
`;

const ExperienceLabel = styled.h2`
  font-family: Rubik;
  font-style: normal;
  font-weight: 500;
  font-size: 28px;
  line-height: 33px;
  color: #2d3639;

  border-bottom: 3px solid #2d3639;
  width: 450px;
  margin: 0px;
`;

type CVExperienceProps = {
  editMode: boolean;
  setEditMode: (mode: boolean) => void;
};

export default function CVExperience({
  editMode,
  setEditMode,
}: CVExperienceProps) {
  const [experienceModules, setExperienceModules] = useState([
    { key: nanoid() },
    { key: nanoid() },
  ]);
  const [activeModule, setActiveModule] = useState("");
  const ref = useRef() as React.MutableRefObject<HTMLDivElement>;

  useClickAway(ref, () => {
    if (editMode === true) {
      setEditMode(false);
      setActiveModule("");
    }
  });

  return experienceModules.length > 0 ? (
    <ExperienceHolder ref={ref}>
      <ExperienceLabel>EXPERIENCE</ExperienceLabel>
      {experienceModules.map((entry) => (
        <ExperienceModule
          key={entry.key}
          activeModule={() => {
            setActiveModule(entry.key);
            setEditMode(true);
          }}
          addNewEntry={() => {
            const arr = [...experienceModules];
            const idx = arr.findIndex((module) => module.key === activeModule);
            const newEntryKey = nanoid();
            arr.splice(idx + 1, 0, { key: newEntryKey });
            arr[idx] = { key: arr[idx].key };
            setActiveModule(newEntryKey);
            setExperienceModules(arr);
          }}
          removeEntry={() => {
            const arr = [...experienceModules];
            const idx = arr.findIndex((module) => module.key === activeModule);
            arr.splice(idx, 1);
            setActiveModule("");
            setEditMode(false);
            setExperienceModules(arr);
          }}
          isActive={entry.key === activeModule}
        />
      ))}
    </ExperienceHolder>
  ) : null;
}
