import styled from "styled-components";

import CheckIcon from "../../common/svg/CheckIcon";

const CVHeaderHolder = styled.div`
  display: flex;
  margin-bottom: 32px;
`;

const ResumeNameLabel = styled.p`
  font-family: Inter;
  font-style: normal;
  font-weight: 600;
  font-size: 14px;
  line-height: 20px;
  color: #828282;
`;

const ResumeNameValue = styled.input`
  margin-left: 6px;
  font-family: Inter;
  font-style: normal;
  font-weight: 500;
  font-size: 14px;
  line-height: 20px;

  ::placeholder {
    color: #bdbdbd;
  }
`;

const LeftSide = styled.div`
  display: flex;
  align-items: center;
  flex: 1;
`;

const RightSide = styled.div`
  display: flex;
`;

const SavedHolder = styled.div`
  gap: 9px;
  display: flex;
  align-items: center;
`;
const SavedLabel = styled.p`
  font-family: Inter;
  font-style: normal;
  font-weight: normal;
  font-size: 12px;
  line-height: 20px;
`;
const LastEditLabel = styled.p`
  font-family: Inter;
  font-style: normal;
  font-weight: normal;
  font-size: 12px;
  line-height: 20px;
  color: #bdbdbd;
  margin-left: 14px;
`;

export default function CVHeadInfo() {
  return (
    <CVHeaderHolder>
      <LeftSide>
        <ResumeNameLabel>Resume name:</ResumeNameLabel>
        <ResumeNameValue placeholder="Your resume" />
      </LeftSide>
      <RightSide>
        <SavedHolder>
          <SavedLabel>Saved</SavedLabel>
          <CheckIcon />
        </SavedHolder>
        <LastEditLabel>Last edited just now</LastEditLabel>
      </RightSide>
    </CVHeaderHolder>
  );
}
