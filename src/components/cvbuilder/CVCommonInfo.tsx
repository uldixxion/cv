import styled from "styled-components";

import EmailIcon from "../../common/svg/EmailIcon";
import LinkIcon from "../../common/svg/LinkIcon";
import LocationIcon from "../../common/svg/LocationIcon";
import PhoneIcon from "../../common/svg/PhoneIcon";
import CVAvatarIcon from "../../common/svg/CVAvatarIcon";
import InputWithIcon from "../../common/ui/InputWithIcon";

const CVCommonHolder = styled.div`
  display: flex;
  justify-content: space-between;
`;
const LeftSide = styled.div``;
const RightSide = styled.div``;

const Name = styled.input`
  font-family: Rubik;
  font-style: normal;
  font-weight: 500;
  font-size: 42px;
  line-height: 50px;
  color: #2d3639;
  margin: 0px;

  ::placeholder {
    color: #2d3639;
  }
`;

const DesiredRole = styled.input`
  font-family: Rubik;
  font-style: normal;
  font-weight: 500;
  font-size: 21px;
  line-height: 25px;
  color: #3f9cdc;

  ::placeholder {
    color: #3f9cdc;
  }
`;

const AvatarHolder = styled.div`
  width: 139px;
  height: 139px;
  border-radius: 50%;
  overflow: hidden;
`;

const CommonData = styled.div`
  display: grid;
  grid-template-columns: repeat(2, 1fr);
  margin: 20px 0;
`;

export default function CVCommonInfo() {
  return (
    <CVCommonHolder>
      <LeftSide>
        <Name placeholder="YOUR NAME" />
        <DesiredRole placeholder="Your next desired role?" />
        <CommonData>
          <InputWithIcon Icon={<PhoneIcon />} placeholder="Phone" />
          <InputWithIcon
            type="email"
            Icon={<EmailIcon />}
            placeholder="Email"
          />
          <InputWithIcon
            type="url"
            Icon={<LinkIcon />}
            placeholder="Website/Link"
          />
          <InputWithIcon
            type="url"
            Icon={<LocationIcon />}
            placeholder="Location"
          />
        </CommonData>
      </LeftSide>
      <RightSide>
        <AvatarHolder>
          <CVAvatarIcon />
        </AvatarHolder>
      </RightSide>
    </CVCommonHolder>
  );
}
