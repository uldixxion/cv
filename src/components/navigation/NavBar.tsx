import styled from "styled-components";

import ArrowDownIcon from "../../common/svg/ArrowDownIcon";
import LogoIcon from "../../common/svg/LogoIcon";
import NotificationIcon from "../../common/svg/NotificationIcon";
import UserAvatar from "../../common/ui/UserAvatar";
import NavList from "./NavList";

const NavBarHolder = styled.div`
  display: flex;
  height: 60px;
  background: #ffffff;
  padding: 0 16px;
`;

const NavLeftSide = styled.div`
  display: flex;
  flex: 1;
  align-items: center;
`;
const NavRightSide = styled.div`
  display: flex;
  align-items: center;
  gap: 14px;
`;

export default function NavBar() {
  return (
    <NavBarHolder>
      <NavLeftSide>
        <LogoIcon />
        <NavList />
      </NavLeftSide>
      <NavRightSide>
        <NotificationIcon />
        <UserAvatar />
        <ArrowDownIcon />
      </NavRightSide>
    </NavBarHolder>
  );
}
