import styled from "styled-components";

const NavListHolder = styled.ul`
  display: flex;
  padding: 0 9px;
`;
const NavListItem = styled.li`
  list-style: none;
`;
const Link = styled.a`
  text-decoration: none;
  padding: 0 15px;

  font-family: Inter;
  font-style: normal;
  font-weight: normal;
  font-size: 14px;
  line-height: 20px;

  color: #232528;
`;

export default function NavList() {
  return (
    <NavListHolder>
      <NavListItem>
        <Link href="/">Dashboard</Link>
      </NavListItem>
      <NavListItem>
        <Link href="/">Help</Link>
      </NavListItem>
    </NavListHolder>
  );
}
