import { createGlobalStyle } from "styled-components";

import CVBuilder from "./components/cvbuilder/CVBuilder";
import NavBar from "./components/navigation/NavBar";

const GlobalStyle = createGlobalStyle`
  body {
    background: #E5E5E5;
    margin: 0;
  }

  p, h1, h2, h3, h4, h5 {
    margin: 0;
  }

  input {
    border: none;
    background: transparent;
    outline: 0;
  }
  `;

function App() {
  return (
    <>
      <GlobalStyle />
      <NavBar />
      <CVBuilder />
    </>
  );
}

export default App;
