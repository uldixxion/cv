import styled from "styled-components";

const InputHolder = styled.div`
  display: flex;
  align-items: center;
  gap: 5px;
  width: 250px;
`;

const StyledInput = styled.input`
  font-family: Inter;
  font-style: normal;
  font-weight: bold;
  font-size: 12px;
  line-height: 20px;
  color: #384347;
  background: inherit;
`;

type InputWithIconProps = {
  Icon: JSX.Element;
  placeholder: string;
  type?: "text" | "email" | "url";
};

export default function InputWithIcon({
  Icon,
  placeholder,
  type,
}: InputWithIconProps) {
  return (
    <InputHolder>
      {Icon}
      <StyledInput type={type} placeholder={placeholder} />
    </InputHolder>
  );
}

InputWithIcon.defaultProps = {
  type: "text",
};
