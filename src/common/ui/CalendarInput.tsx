import styled from "styled-components";

const InputHolder = styled.div`
  display: flex;
  align-items: center;
  gap: 6px;
  input[type="date"]::-webkit-inner-spin-button,
  input[type="date"]::-webkit-calendar-picker-indicator {
    display: none;
    -webkit-appearance: none;
  }
`;

const StyledInput = styled.input`
  font-family: Inter;
  font-style: normal;
  font-weight: bold;
  font-size: 12px;
  line-height: 20px;
  color: #384347;
  background: inherit;
`;

type InputWithIconProps = {
  Icon: JSX.Element;
  placeholder: string;
};

export default function CalendarInput({
  Icon,
  placeholder,
}: InputWithIconProps) {
  return (
    <InputHolder>
      {Icon}
      <StyledInput type="date" placeholder={placeholder} />
    </InputHolder>
  );
}
