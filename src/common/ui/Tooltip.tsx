import {
  DetailedHTMLProps,
  HTMLAttributes,
  PropsWithChildren,
  useState,
} from "react";
import styled from "styled-components";

const ToolTipStyles = styled.div`
  --tooltip-text-color: black;
  --tooltip-background-color: #fff;
  --tooltip-arrow-size: 6px;

  display: inline-block;
  position: relative;

  .Tooltip-Tip {
    position: absolute;
    border-radius: 4px;
    left: 0;
    padding: 6px;
    color: var(--tooltip-text-color);
    background: var(--tooltip-background-color);
    font-size: 14px;
    z-index: 100;
    width: 290px;
    box-shadow: 0px 2px 6px rgb(0 0 0 / 10%);
    filter: drop-shadow(0px 2px 20px rgba(0, 0, 0, 0.2));
  }

  .Tooltip-Tip::before {
    content: "";
    left: 24px;
    border: solid transparent;
    height: 0;
    width: 0;
    position: absolute;
    pointer-events: none;
    border-width: var(--tooltip-arrow-size);
    margin-left: calc(var(--tooltip-arrow-size) * -1);
  }

  .Tooltip-Tip.top {
    top: 20px;
  }
  .Tooltip-Tip.top::before {
    top: -12px;
    border-bottom-color: var(--tooltip-background-color);
  }
`;

type Tooltiprops = PropsWithChildren<{
  content: DetailedHTMLProps<HTMLAttributes<HTMLDivElement>, HTMLDivElement>;
}>;

export default function Tooltip({ children, content }: Tooltiprops) {
  let timeout: NodeJS.Timeout;
  const [active, setActive] = useState(false);

  const showTip = () => {
    timeout = setTimeout(() => {
      setActive(true);
    }, 400);
  };

  const hideTip = () => {
    clearInterval(timeout);
    setActive(false);
  };

  return (
    <ToolTipStyles onMouseEnter={showTip} onMouseLeave={hideTip}>
      {children}
      {active && <div className="Tooltip-Tip top">{content}</div>}
    </ToolTipStyles>
  );
}
