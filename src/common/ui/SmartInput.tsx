import { useEffect, useState } from "react";
import ContentEditable from "react-contenteditable";
import styled from "styled-components";
import sanitizeHtml from "sanitize-html";

import Tooltip from "./Tooltip";
import BulletListIcon from "../svg/BulletListIcon";

const sanitizeConf = {
  allowedTags: ["b", "i", "u", "mark"],
};

const StyledInput = styled(ContentEditable)`
  :empty::before {
    content: attr(placeholder);
    display: block;
    color: #aaa;
  }

  outline: none;

  font-family: Inter;
  font-style: normal;
  font-weight: 500;
  font-size: 13px;
  line-height: 20px;
  color: #384347;

  mark {
    background: rgba(235, 87, 87, 0.66);
  }
`;

const TooltipContentHolder = styled.div`
  padding: 12px 16px 12px 16px;
  flex-direction: column;
`;
const TooltipHeader = styled.div`
  display: flex;
  align-items: center;
`;
const TooltipHeaderLabel = styled.h4`
  font-family: Inter;
  font-style: normal;
  font-weight: bold;
  font-size: 12px;
  line-height: 20px;
  display: flex;
  align-items: center;
  color: #232528;
  margin: 0 11px;
`;

const CheckboxLabel = styled.label`
  display: flex;
  margin-left: 32px;
  gap: 7px;
`;

const TooltipMessage = styled.p`
  margin-top: 12px;
  font-family: Inter;
  font-style: normal;
  font-weight: normal;
  font-size: 12px;
  line-height: 20px;
  display: flex;
  align-items: center;
  color: #232528;
`;

const CheckboxInput = styled.input`
  border: 1px solid #bdbdbd;
  box-sizing: border-box;
  border-radius: 3px;
`;
const CheckboxMessage = styled.p`
  font-family: Rubik;
  font-style: normal;
  font-weight: normal;
  font-size: 12px;
  line-height: 20px;
  display: flex;
  align-items: flex-end;
  color: #232528;
`;

const caMock = [
  // {
  //   range: [10, 23],
  //   message:
  //     "This is vague. Instead of “Managed projects for many clients”, say “Managed projects for 10 clients including BlueBank.”",
  // },
  {
    range: [0, 23],
    message:
      'Include a valuable metric if possible. For example: "Increased revenue by 20% within one month.".',
  },
];

type TooltipContentProps = {
  setText: React.Dispatch<
    React.SetStateAction<{
      html: string;
      state: "notset" | "ignored" | "corrected";
      seen: boolean;
    }>
  >;
  text: {
    html: string;
    state: "notset" | "ignored" | "corrected";
    seen: boolean;
  };
};
function TooltipContent({ setText, text }: TooltipContentProps) {
  const [checked, setChecked] = useState(false);

  useEffect(() => {
    setText({ ...text, seen: true });
    // Note: We want to trigger only once on init
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const handleChange = () => {
    let removeMarkText = text.html.replace("<mark>", "");
    removeMarkText = removeMarkText.replace("</mark>", "");
    setChecked(true);
    setText({ ...text, html: removeMarkText, state: "ignored" });
  };

  return (
    <TooltipContentHolder>
      <TooltipHeader>
        <BulletListIcon />
        <TooltipHeaderLabel>Content Improvement</TooltipHeaderLabel>
        {/* eslint-disable-next-line jsx-a11y/label-has-associated-control */}
        <CheckboxLabel>
          <CheckboxInput
            type="checkbox"
            checked={checked}
            onChange={handleChange}
          />
          <CheckboxMessage>Ignore</CheckboxMessage>
        </CheckboxLabel>
      </TooltipHeader>
      <TooltipMessage>{caMock[0].message}</TooltipMessage>
    </TooltipContentHolder>
  );
}

type SmartInputProps = {
  placeholder: string;
};

function appendCA(str: string) {
  if (!str.startsWith("I've done many projects")) {
    return sanitizeHtml(str, sanitizeConf);
  }
  const path = [];
  for (let m = 0; m < caMock.length; m += 1) {
    path.push(...caMock[m].range);
  }
  path.sort((a, b) => a - b);

  const caText = `<mark>${str.slice(
    path[0],
    path[path.length - 1]
  )}</mark>${str.slice(path[path.length - 1])}`;

  return sanitizeHtml(caText, sanitizeConf);
}

// Input with bold/italic/underscore/mark functionality
export default function SmartInput({ placeholder }: SmartInputProps) {
  const [text, setText] = useState({
    html: appendCA("I've done many projects. Test123"),
    state: "notset" as "ignored" | "corrected" | "notset",
    seen: false,
  });

  const handleChange = (evt: { target: { value: string } }) => {
    const html = appendCA(evt.target.value);
    // Sanitize to prevent XSS attacks
    setText({ ...text, html });
  };

  return text.state === "ignored" ? (
    <>
      <StyledInput
        html={text.html}
        onChange={handleChange}
        placeholder={placeholder}
      />
    </>
  ) : (
    <>
      <Tooltip content={<TooltipContent setText={setText} text={text} />}>
        <StyledInput
          html={text.html}
          onChange={handleChange}
          placeholder={placeholder}
        />
      </Tooltip>
    </>
  );
}
