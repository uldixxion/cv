import styled from "styled-components";

import UserAvatarIcon from "../svg/UserAvatarIcon";

const AvatarHolder = styled.div`
  width: 40px;
  height: 40px;
  box-shadow: 0px 1px 3px rgba(0, 0, 0, 0.1);
  border-radius: 50%;
`;

export default function UserAvatar() {
  return (
    <AvatarHolder>
      <UserAvatarIcon />
    </AvatarHolder>
  );
}
